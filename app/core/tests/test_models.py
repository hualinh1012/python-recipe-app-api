from django.test import TestCase
from django.contrib.auth import get_user_model


class ModelTests(TestCase):

    def test_create_user_with_email_successful(self):
        """Test creating new user"""
        email = 'test@gmail.com'
        password = '1qaz!QAZ'
        user = get_user_model().objects.create_user(
            email,
            password
        )
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        email = 'test@Gmail.Com'
        user = get_user_model().objects.create_user(
            email,
            'test123'
        )
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(
                None,
                'test123'
            )

    def test_create_new_super_user(self):
        user = get_user_model().objects.create_superuser(
            'hualinh1012@gmail.com',
            '1qaz!QAZ'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
